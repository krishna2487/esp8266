#include <ESP8266WiFi.h>
#include "Wire.h"
#include "TSL2561.h"
#include "ArduinoJson.h"

const char* ssid     = "Leapcraft";
const char* password = "1bigleap";

const char* host = "ingestion-dev.leapcraft.dk";

char str[512], str1[512];
TSL2561 tsl(TSL2561_ADDR_FLOAT);



void setup() {

  Serial.begin(115200);
  delay(100);
  pinMode(2, OUTPUT);
  pinMode(5, OUTPUT);
  digitalWrite(2, HIGH);
  digitalWrite(15, LOW);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

 while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  if (tsl.begin()) {
    Serial.println("Found sensor");
  } else {
    Serial.println("No sensor?");
    while (1);
  }

  // You can change the gain on the fly, to adapt to brighter/dimmer light situations
  //tsl.setGain(TSL2561_GAIN_0X);         // set no gain (for bright situtations)
  tsl.setGain(TSL2561_GAIN_16X);      // set 16x gain (for dim situations)

  // Changing the integration time gives you a longer time over which to sense light
  // longer timelines are slower, but are good in very low light situtations!
  tsl.setTiming(TSL2561_INTEGRATIONTIME_13MS);  // shortest integration time (bright light)
  //tsl.setTiming(TSL2561_INTEGRATIONTIME_101MS);  // medium integration time (medium light)
  //tsl.setTiming(TSL2561_INTEGRATIONTIME_402MS);  // longest integration time (dim light)

  // Now we're ready to get readings!
}

void loop() {

  uint16_t VISIBLE_SPECTRUM = tsl.getLuminosity(TSL2561_VISIBLE);
  //uint16_t x = tsl.getLuminosity(TSL2561_FULLSPECTRUM);
  //uint16_t x = tsl.getLuminosity(TSL2561_INFRARED);

  Serial.println(VISIBLE_SPECTRUM, DEC);

  uint32_t FULL_LUMINOSITY = tsl.getFullLuminosity();
  uint16_t IR_SPECTRUM, FULL_SPECTRUM;
  IR_SPECTRUM = FULL_LUMINOSITY >> 16;
  FULL_SPECTRUM = FULL_LUMINOSITY & 0xFFFF;
  Serial.print("IR: "); Serial.println(IR_SPECTRUM);
  Serial.print("Full: "); Serial.println(FULL_SPECTRUM);
  Serial.print("Visible: "); Serial.println(FULL_SPECTRUM - IR_SPECTRUM);
  Serial.print("Lux: "); Serial.println(tsl.calculateLux(FULL_SPECTRUM, IR_SPECTRUM));


  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }

  StaticJsonBuffer<800> jsonBuffer;
	JsonObject& root = jsonBuffer.createObject();
  root["app"] = "cphsense";
  root["id"] = "00:02:f7:f0:00:00";
  root["board_id"] = "null";
  root["time"] = "2017-07-12T16:07:00+00:00";
  JsonObject& payload = root.createNestedObject("payload");
  payload["event"]="periodic";
  payload["IRSpectrum"]=IR_SPECTRUM;
  payload["FullSpectrum"]=FULL_SPECTRUM;
  payload["VISIBLE_SPECTRUM"]=(FULL_SPECTRUM-IR_SPECTRUM);
  payload["LUX"]=tsl.calculateLux(FULL_SPECTRUM, IR_SPECTRUM);
  payload["batt"]=0;
  payload["pm2_5"]=0;
  payload["pm1"]=0;
  payload["pm10"]=0;
  payload["opened"]=1;
  root["type"]="report";

  root.printTo(str);

   Serial.print("Requesting POST: ");
   // Send request to the server:
   client.println("POST / HTTP/1.1");
   client.println("Host: ingestion-dev.leapcraft.dk");
   client.println("Accept: */*");
   client.println("Content-Type: application/json");
   client.print("Content-Length: ");
   client.println(root.measureLength());
   client.println();
   client.print(str);


   delay(500); // Can be changed

   Serial.println("Response: \n");
    while(client.available()){
        String line = client.readStringUntil('\r');
        Serial.print(line);
    }

  if (client.connected()) {
    client.stop();  // DISCONNECT FROM THE SERVER
  }
  Serial.println();
  Serial.println("closing connection");
  delay(5000);
  delay(5000);
  delay(5000);
  delay(5000);
}
